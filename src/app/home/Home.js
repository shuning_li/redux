import React from 'react';
import {Link} from "react-router-dom";

const Home = () => {
  return (
    <div>
      <h3>This is a beautiful home page.</h3>
      <ul>
        <li>
          <Link to='/product-details'>Go to Product Details Page</Link>
        </li>
      </ul>
      <h3>User Profile</h3>
      <ul>
        <li><Link to='/user-profiles/1'>Go to User Profiles Page</Link></li>
        <li><Link to='/user-profiles/2'>Go to User Profiles Page</Link></li>
      </ul>
    </div>
  )
};

export default Home;