export const setUserProfile = id => dispatch => {
  fetch(`http://localhost:8080/api/user-profiles/${id}`)
    .then(res => res.json())
    .then(result => {
      dispatch({
        type: 'SET_USER_PROFILE',
        profile: result
      })
    })
};

export const setLikeStatus = data => dispatch => {
  fetch('http://localhost:8080/api/like', {
    method: 'POST',
    headers: { 'content-type': 'application/json' },
    body: JSON.stringify(data)
  }).then(() => {
      dispatch({
        type: 'SET_LIKE_STATUS',
        like: true
      })
    })
};