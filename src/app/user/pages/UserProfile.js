import React from 'react';
import Profile from '../components/Profile';
import { bindActionCreators } from 'redux';
import { setLikeStatus, setUserProfile } from '../actions/userActions';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class UserProfile extends React.Component {

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.setUserProfile(id);
  }

  handleClick() {
    const { id } = this.props.match.params;
    const data = { userProfileId: id, userName: 'anonymous user' };
    this.props.setLikeStatus(data);
  }

  render() {
    const { name, gender, description, like } = this.props;
    return (
      <div>
        <h3>User Profile</h3>
        <Profile name={name} gender={gender} description={description}/>
        <section>
          <button onClick={this.handleClick.bind(this)} disabled={like}>{like ? 'liked' : 'like'}</button>
          <Link to="/">back to home</Link>
        </section>
      </div>
    )
  }

}

const mapStateToProps = state => state.user;
const mapDispatchToProps = dispatch => bindActionCreators({
  setUserProfile,
  setLikeStatus
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);