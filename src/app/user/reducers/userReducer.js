const initialState = {};


export default (state = initialState, action) => {
  switch (action.type) {
    case 'SET_USER_PROFILE':
      return { ...state, ...action.profile };
    case 'SET_LIKE_STATUS':
      return {...state, like: action.like}
    default:
      return state;
  }
}